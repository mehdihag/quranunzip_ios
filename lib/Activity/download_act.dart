import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qoranios/Activity/surelist_act.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';

class download_act extends StatefulWidget {
  @override
  _download_actState createState() => _download_actState();
}

class _download_actState extends State<download_act>
{
  bool downloading = false;
  String progress = '0';
  bool isDownloaded = false;
  /*String uri = 'http://nahjo.ir/mobile/sound/1.mp3'; // url of the file to be downloaded
  String filename = 'testaudio1.mp3'; // file name that you desire to keep*/

  String uri = 'https://s1.yousefi.ir/files/1.zip'; // url of the file to be downloaded
  String filename = 'testsound.zip'; // file name that you desire to keep

  String img_path="assets/images/kadr_name.jpg";
  File f=File("data/data/com.hamrayan.qoranios/app_flutter");
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home:
    Stack(children:
    [
      Container(color: Colors.white,),
      Container(margin:EdgeInsets.fromLTRB(5.0, 40.0, 0, 0) ,child: RaisedButton(onPressed: downfile,child: Text("down dio"),)),
      Container(margin:EdgeInsets.fromLTRB(5.0, 80.0, 0, 0) ,child: Text('$progress%')),
      Container(margin:EdgeInsets.fromLTRB(5.0, 180.0, 0, 0) ,child: Image.file(f,height: 200,fit: BoxFit.fill,)),
    ],),);
  }
  downfile() async
  {
    print("aaa");
    String dir = (await getApplicationDocumentsDirectory()).path;
    print("dir="+dir);//data/data/com.hamrayan.qoranios/app_flutter
    downloadFile(uri, filename);
  }
  Future<void> downloadFile(uri, fileName) async {
    setState(() {
      downloading = true;
    });

    String savePath = await getFilePath(fileName);
    print("savePath=$savePath");
    f=File(savePath);
    Dio dio = Dio();

    dio.download(
      uri,
      savePath,
      onReceiveProgress: (rcv, total) {
        print(
            'received: ${rcv.toStringAsFixed(0)} out of total: ${total.toStringAsFixed(0)}');

        setState(() {
          progress = ((rcv / total) * 100).toStringAsFixed(0);
        });

        if (progress == '100') {
          setState(() {
            isDownloaded = true;
          });
        } else if (double.parse(progress) < 100) {}
      },
      deleteOnError: true,
    ).then((_) {
      setState(() {
        if (progress == '100') {
          isDownloaded = true;
        }

        downloading = false;
      });
    });
  }
  Future<String> getFilePath(uniqueFileName) async {
    String path = '';

    Directory dir = await getApplicationDocumentsDirectory();

    path = '${dir.path}/$uniqueFileName';
    //path = '${dir.path}/assets/images/$uniqueFileName';

    return path;
  }
}
